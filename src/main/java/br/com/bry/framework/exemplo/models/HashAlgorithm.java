package br.com.bry.framework.exemplo.models;

public enum HashAlgorithm {

	SHA1, SHA256, SHA512

}
