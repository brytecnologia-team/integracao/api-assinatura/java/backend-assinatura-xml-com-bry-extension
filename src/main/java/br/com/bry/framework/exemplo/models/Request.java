package br.com.bry.framework.exemplo.models;

public class Request {

	private String nonce;
	private boolean attached;
	private String profile;
	private String hashAlgorithm;
	private String certificate;
	private String operationType;
	private String documentNonce;

	// 'originalDocuments[0][content]': file
	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public boolean isAttached() {
		return attached;
	}

	public void setAttached(boolean attached) {
		this.attached = attached;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getHashAlgorithm() {
		return hashAlgorithm;
	}

	public void setHashAlgorithm(String hashAlgorithm) {
		this.hashAlgorithm = hashAlgorithm;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getDocumentNonce() {
		return documentNonce;
	}

	public void setDocumentNonce(String documentNonce) {
		this.documentNonce = documentNonce;
	}

}
