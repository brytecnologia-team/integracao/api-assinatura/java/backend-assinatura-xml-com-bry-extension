package br.com.bry.framework.exemplo.models;

import java.math.BigInteger;

public class Signature {

	protected BigInteger nonce;

	protected String content;

	public Signature() {

	}

	public Signature(BigInteger nonce, String content) {
		setNonce(nonce);
		setContent(content);
	}

	public void setNonce(BigInteger nonce) {
		this.nonce = nonce;
	}

	public void setContent(String conteudo) {
		this.content = conteudo;
	}

	public BigInteger getNonce() {
		return nonce;
	}

	public String getContent() {
		return content;
	}

	@Override
	public String toString() {
		return "Signature [" + "nonce=" + nonce + ", content=" + content + "]";
	}
}